# App Expo para estudos
```
cd doctor-rocket
npm i 
npx expo start
```

## Observação
Até o momento, a versão do styled-components no package.json na versão 5.3.11 esta funcionando e a versão 6, tem conflitos para o autocomplete ao usar a variavel theme no css.

Até o momento, para usar o styled-components, seguir os seguintes commands

```
yarn add styled-components@5.3.11

yarn add -D @types/styled-components-react-native
```