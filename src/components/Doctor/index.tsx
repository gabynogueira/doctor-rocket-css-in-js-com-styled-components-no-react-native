import { 
    Container, 
    Avatar, 
    Name, 
    Specialist,
    DoctorProps,
} from "./styled";

export type DoctorDataprops = {
    id: string;
    avatar: string;
    name: string;    
    specialist: string; 
}

type Props = DoctorProps & {
    data: DoctorDataprops;
}

export function Doctor({ type, data }: Props) {
    return(
        <Container type={type}>
            <Avatar source={{uri: data.avatar}} />
            <Name>
                {data.name}
            </Name>

            <Specialist>
                {data.specialist}
            </Specialist>
        </Container>
    );
}