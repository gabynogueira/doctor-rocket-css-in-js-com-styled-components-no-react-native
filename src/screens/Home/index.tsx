
import { Container, List } from "./styles";

import { Doctor } from "../../components/Doctor";


const DATA = [
    {
        id:'1', 
        avatar:"https://avatars.githubusercontent.com/u/84452944?v=4", 
        name:'Gabriela Nogueira', 
        specialist:'Pediatra'
    },
    {id:'2', avatar:"https://avatars.githubusercontent.com/u/84452944?v=4", name:'Gabriela Nogueira', specialist:'Pediatra'},
    {id:'3', avatar:"https://avatars.githubusercontent.com/u/84452944?v=4", name:'Gabriela Nogueira', specialist:'Pediatra'},
    {id:'4', avatar:"https://avatars.githubusercontent.com/u/84452944?v=4", name:'Gabriela Nogueira', specialist:'Pediatra'},
    {id:'5', avatar:"https://avatars.githubusercontent.com/u/84452944?v=4", name:'Gabriela Nogueira', specialist:'Pediatra'},
    {id:'6', avatar:"https://avatars.githubusercontent.com/u/84452944?v=4", name:'Gabriela Nogueira', specialist:'Pediatra'},
    {id:'7', avatar:"https://avatars.githubusercontent.com/u/84452944?v=4", name:'Gabriela Nogueira', specialist:'Pediatra'},
    {id:'8', avatar:"https://avatars.githubusercontent.com/u/84452944?v=4", name:'Gabriela Nogueira', specialist:'Pediatra'},
];


export function Home(){
    return(
        <Container>
            <List 
                data={DATA}
                keyExtractor={ item => item.id }
                renderItem={({ item }) => <Doctor type="primary" data={item}/>}
                numColumns={2}
            />
                
        </Container>
    )
}